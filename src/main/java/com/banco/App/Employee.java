package com.banco.App;

public class Employee {
	
	private String function;
	private String fullname;
	
	public Employee() {
		super();
	}
	
	public String getFullname() {
		return fullname;
	}
	public String getFunction() {
		return function;
	}
	
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	
	public Employee(String fullname, String function) {
		super();
		this.fullname = fullname;
		this.function = function;
	}

	

	public void setBoss(Employee boss) {
		
	}

}
