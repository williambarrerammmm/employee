package com.banco.App;

import java.util.LinkedList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest")
public class restController {
	
	List<Employee> employees = new LinkedList<>();

	@GetMapping("/add")
	public Employee addEmployee(@RequestParam(value = "fullname", defaultValue = "World") String name) {
		
		Employee employee = new Employee("Employee", name);
		employees.add(employee);
		return employee;
	}
	
	@GetMapping("/list")
	public List<Employee> listEmployee() {
		return employees;
	}

}
