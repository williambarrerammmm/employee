package com.banco.crud;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;

import org.springframework.web.context.annotation.RequestScope;

import com.banco.App.Employee;

@ManagedBean(value = "employeeBean")
@RequestScope
public class EmployeeBean {
	
	private List<Employee> Employees;
	private EmployeePersistence persistence;
	public String message = "hello world";
	private Employee Employee = new Employee();
	
	public EmployeePersistence getPersistence() {
		return persistence;
	}
	public void setPersistence(EmployeePersistence persistence) {
		this.persistence = persistence;
	}
	public Employee getBook() {
		return Employee;
	}
	public void setBook(Employee Employee) {
		this.Employee = Employee;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public EmployeeBean() {
		persistence = new EmployeePersistence();
		Employees= new ArrayList<Employee>();
	}
	@PostConstruct
	public void init() {
		this.findAllEmployees();
	}
	
	public void findAllEmployees() {
		Employees = persistence.fillEmployees();
	}
	
	public List<Employee> getEmployees() {
		return Employees;
	}
	
	public void setBooks(List<Employee> Employees) {
		this.Employees = Employees;
	}
	public String saveEmployee() {
		String navigationResult="";
		int saveResult = persistence.saveEmployee(Employee);
		if(saveResult !=0) {
			navigationResult="bookList.xhtml?faces-redirect=true";
		}else {
			navigationResult="createBook.xhtml?faces-redirect=true";
		} 
		return navigationResult;
	}
	


}
