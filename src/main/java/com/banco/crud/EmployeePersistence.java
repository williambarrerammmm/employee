package com.banco.crud;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.banco.App.Employee;

public class EmployeePersistence {

	private List<Employee> employees;

	public static Statement stmtObj;
	public static Connection connObj;
	public static ResultSet resultSetObj;
	public static ResultSet resultGetObj;
	public static PreparedStatement pstmt;

	public Connection getConnection() {
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			String db_url = "jdbc:mariadb://localhost:3307/employee", db_userName = "root", db_password = "1234";
			connObj = DriverManager.getConnection(db_url, db_userName, db_password);
		} catch (Exception sqlException) {
			sqlException.printStackTrace();
		}
		return connObj;
	}

	public EmployeePersistence() {
		super();
		employees = new ArrayList<Employee>();
		this.employees = this.fillEmployees();
	}

	public void addEmployee(Employee employee) {
		this.employees.add(employee);
	}
	
	public List<Employee> fillEmployees() {
		List<Employee> employeeList = new ArrayList<Employee>();
		String sql = "select * from employee";
		try{
			stmtObj = getConnection().createStatement();
			resultSetObj = stmtObj.executeQuery(sql);
			while (resultSetObj.next()) {
				Employee employee = new Employee();
				employee.setFullname(resultSetObj.getString("fullname"));
				employee.setFunction(resultSetObj.getString("function"));
				employeeList.add(employee);
				
			}
			System.out.println("Total "+ employeeList.size());
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return employeeList;
	}
	
	public int saveEmployee(Employee employee) {
		int saveResult=0;
		try {
			pstmt = getConnection().prepareStatement("insert into employee(fullname, function) values (?,?)");
			pstmt.setString(1, employee.getFullname());
			pstmt.setString(2, employee.getFunction());
			saveResult =pstmt.executeUpdate();
			connObj.close();
		}catch (Exception sqlException) {
			sqlException.printStackTrace();
		}
		return saveResult;
	}
	
	public List<Employee> getEmployee(){
		return employees;
	}

}
