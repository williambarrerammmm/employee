package com.banco.crud;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.banco.App.Employee;

@WebService
public class EmployeeCrudWS {

	public EmployeePersistence persistence = new EmployeePersistence();
	
	//Mostrar toda la lista 
		@WebMethod
		public List<Employee>allEmployee(){
			return persistence.fillEmployees();
		}
		
		
		@WebMethod
		public String create(@WebParam(name ="employee")Employee employee) {
			int saveResult = (int) persistence.saveEmployee(employee);
			String navigationResult="";
			if(saveResult !=0) {
				navigationResult="Employee was created succesfully";
			}else {
				navigationResult="Employee isn´t created";
			} return navigationResult;
		}
		
	
}
